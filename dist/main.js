'use strict';

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pkg = require("../package.json");
var path = require('path');

var program = require('commander');

var _require = require('./utils/common'),
    mapActions = _require.mapActions;
// Object.keys()


Reflect.ownKeys(mapActions).forEach(function (action) {
    program.command(action) //配置命令的名字
    .alias(mapActions[action].alias) // 命令的别名
    .description(mapActions[action].description) // 命令对应的描述
    .action(function () {
        //动作
        if (action === '*') {
            //访问不到对应的命令 就打印找不到命令
            console.log(mapActions[action].description);
        } else {
            // 分解命令 到文件里 有多少文件 就有多少配置 create config
            // lee-cli create project-name ->[node,lee-cli,create,project-name]
            require(path.join(__dirname, action)).apply(undefined, (0, _toConsumableArray3.default)(process.argv.slice(3)));
        }
    });
});

program.version(pkg.version).parse(process.argv);