'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _ora = require('ora');

var _ora2 = _interopRequireDefault(_ora);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// 根据我们想要实现的功能配置执行动作，遍历产生对应的命令
var mapActions = {
    create: {
        alias: 'c', //别名
        description: '创建一个项目', // 描述
        examples: [//用法
        'leoy-cli create <project-name>']
    },
    config: { //配置文件
        alias: 'conf', //别名
        description: 'config project variable', // 描述
        examples: [//用法
        'leoy-cli config set <k> <v>', 'leoy-cli config get <k>']
    },
    '*': {
        alias: '', //别名
        description: 'command not found', // 描述
        examples: [] //用法
    }
};

var loadByOra = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(fn, msg) {
        var spinner, res;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        spinner = (0, _ora2.default)(msg);

                        spinner.start();
                        res = void 0;
                        _context.prev = 3;
                        _context.next = 6;
                        return fn();

                    case 6:
                        res = _context.sent;

                        spinner.succeed();
                        _context.next = 14;
                        break;

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](3);

                        console.log('请求出错，', _context.t0.code);
                        spinner.fail('请求失败');

                    case 14:
                        return _context.abrupt('return', res);

                    case 15:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[3, 10]]);
    }));

    return function loadByOra(_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();

module.exports = {
    mapActions: mapActions,
    loadByOra: loadByOra
};