'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _ora = require('ora');

var _ora2 = _interopRequireDefault(_ora);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var inquirer = require('inquirer');

var download = require('download-git-repo');

var _require = require('./utils/common'),
    loadByOra = _require.loadByOra;

var chooseArr = ['react', 'vue', 'vue TS', 'vue mutiple page(多页面)'];

// 这个下载url格式时固定的 github:${git用户名}/${仓库名称}
// const reactUrl = 'direct:https://gitlab.com/leoyyy/clitest.git';
// const reactUrl = 'github:leoyyy3/kingVue';
var reactUrl = 'direct:https://gitlab.com/leoyyy/clitest/-/archive/main/clitest-main.zip';
var vueh5Url = 'direct:https://gitlab.com/leoyyy/vue-h5-template/-/archive/main/vue-h5-template-main.zip';
var vueh5tsUrl = 'direct:https://gitlab.com/leoyyy/vue-h5-ts-template/-/archive/main/vue-h5-ts-template-main.zip';
var vueMutiplePage = 'direct:https://gitlab.com/leoyyy3/vue-multiple-page/-/archive/main/vue-multiple-page-main.zip';

module.exports = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(projectName) {
        var _ref2, repo, url, spinner;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return inquirer.prompt([{
                            type: 'list',
                            name: 'repo',
                            message: '请选择一个你要创建的项目类型',
                            choices: chooseArr
                        }]);

                    case 2:
                        _ref2 = _context.sent;
                        repo = _ref2.repo;
                        url = getTemp(repo);


                        if (url) {
                            spinner = (0, _ora2.default)('正在下载模板...');

                            spinner.start();
                            download(url, projectName, function (err) {
                                if (err) {
                                    spinner.fail('下载失败');
                                } else {
                                    spinner.succeed('下载成功');
                                }
                            });
                        } else {
                            console.warn('暂无相关模板');
                        }

                    case 6:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

function getTemp(re) {
    switch (re) {
        case 'react':
            return reactUrl;
        case 'vue':
            return vueh5Url;
        case 'vue TS':
            return vueh5tsUrl;
        case 'vue mutiple page(多页面)':
            return vueMutiplePage;
        default:
            return '';
    }
}