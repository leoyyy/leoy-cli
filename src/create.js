var inquirer = require('inquirer');
import ora from 'ora'
const download = require('download-git-repo')

const { loadByOra } = require('./utils/common');

const chooseArr = ['react', 'vue', 'vue TS', 'vue mutiple page(多页面)']

// 这个下载url格式时固定的 github:${git用户名}/${仓库名称}
// const reactUrl = 'direct:https://gitlab.com/leoyyy/clitest.git';
// const reactUrl = 'github:leoyyy3/kingVue';
const reactUrl = 'direct:https://gitlab.com/leoyyy/clitest/-/archive/main/clitest-main.zip';
const vueh5Url = 'direct:https://gitlab.com/leoyyy/vue-h5-template/-/archive/main/vue-h5-template-main.zip'
const vueh5tsUrl = 'direct:https://gitlab.com/leoyyy/vue-h5-ts-template/-/archive/main/vue-h5-ts-template-main.zip'
const vueMutiplePage = 'direct:https://gitlab.com/leoyyy3/vue-multiple-page/-/archive/main/vue-multiple-page-main.zip'

module.exports = async (projectName) => {
    const { repo } = await inquirer.prompt([
        {
            type: 'list',
            name: 'repo',
            message: '请选择一个你要创建的项目类型',
            choices: chooseArr
        }
    ]);

    const url = getTemp(repo)

    if(url){
        const spinner = ora('正在下载模板...');
        spinner.start();
        download(url, projectName, function (err) {
            if(err){
                spinner.fail('下载失败')
            }else{
                spinner.succeed('下载成功');
            }
        })
    }else {
        console.warn('暂无相关模板')
    }


};

function getTemp(re){
    switch(re){
        case 'react':
            return reactUrl;
        case 'vue':
            return vueh5Url;
        case 'vue TS':
            return vueh5tsUrl;
        case 'vue mutiple page(多页面)':
            return vueMutiplePage;
        default:
            return ''
    }
}
