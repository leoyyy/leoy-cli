import ora from 'ora'

// 根据我们想要实现的功能配置执行动作，遍历产生对应的命令
const mapActions = {
    create: {
        alias: 'c', //别名
        description: '创建一个项目', // 描述
        examples: [ //用法
            'leoy-cli create <project-name>'
        ]
    },
    config: { //配置文件
        alias: 'conf', //别名
        description: 'config project variable', // 描述
        examples: [ //用法
            'leoy-cli config set <k> <v>',
            'leoy-cli config get <k>'
        ]
    },
    '*': {
        alias: '', //别名
        description: 'command not found', // 描述
        examples: [] //用法
    }
}

const loadByOra = async (fn, msg) => {
    const spinner = ora(msg);
    spinner.start();
    let res;
    try{
        res = await fn();
        spinner.succeed();
    }catch(e){
        console.log('请求出错，', e.code)
        spinner.fail('请求失败')
    }
    
    
    return res
}

module.exports = {
    mapActions,
    loadByOra
};
