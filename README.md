# leoy-cli

初始化前端项目的脚手架命令工具，可提供基于webpack构建的vue，vue-ts，react项目

## Getting start

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

```
npm i leoy-cli -g

```

## get project template init

- [ ] create project with the following command:
```
leoy-cli create projectName
```
- [ ] select project template type: vue, vue-ts, react




